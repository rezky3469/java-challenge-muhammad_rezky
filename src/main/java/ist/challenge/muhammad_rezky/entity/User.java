package ist.challenge.muhammad_rezky.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.Pattern;
import lombok.*;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "password")
    @Pattern(regexp = "^(?=.*[a-zA-Z])(?=.*\\d)(?!\\s).{8,}$", message = "Password must contain at least one letter, one digit, no spaces, and be at least 8 characters long.")
    private String password;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
