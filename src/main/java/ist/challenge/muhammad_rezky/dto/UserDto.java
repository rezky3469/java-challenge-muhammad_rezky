package ist.challenge.muhammad_rezky.dto;
import jakarta.validation.constraints.*;
import lombok.*;
import static ist.challenge.muhammad_rezky.utils.MessageConstants.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    @NotBlank(message = MESSAGE_USERNAME_NOT_BLANK)
    @Size(max = 25, message = MESSAGE_USERNAME_MAX)
    private String username;

    @NotBlank(message = MESSAGE_PASSWORD_NOT_BLANK)
    @Size(max = 25, message = MESSAGE_PASSWORD_MAX)
    private String password;    
}
