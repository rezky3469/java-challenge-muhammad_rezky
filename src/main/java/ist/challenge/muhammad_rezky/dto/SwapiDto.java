package ist.challenge.muhammad_rezky.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SwapiDto {
    private String name;
    private String height;
    private String mass;   
}
