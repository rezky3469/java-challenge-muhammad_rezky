package ist.challenge.muhammad_rezky.services;

import static ist.challenge.muhammad_rezky.utils.MessageConstants.*;
import java.util.*;
import org.springframework.http.*;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import ist.challenge.muhammad_rezky.dto.SwapiDto;

@Service
public class SwapiService {    

    public List<SwapiDto> listPeople() {
        RestTemplate restTemplate = new RestTemplate();
        List<SwapiDto> responses = new ArrayList<>();
        
        ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(
            SWAPI_URL,
            HttpMethod.GET,
            null,
            new ParameterizedTypeReference<Map<String, Object>>() {}
        );

        Map<String, Object> response = responseEntity.getBody();

        if (response == null) {
            return Collections.emptyList();
        }

        @SuppressWarnings("unchecked")
        List<Map<String, Object>> results = (List<Map<String, Object>>) response.get("results");

        for (Map<String, Object> result : results) {
            responses.add(SwapiDto.builder()
                    .name((String) result.get("name"))
                    .height((String) result.get("height"))
                    .mass((String) result.get("mass"))
                    .build());
        }

        return responses;
    }
}
