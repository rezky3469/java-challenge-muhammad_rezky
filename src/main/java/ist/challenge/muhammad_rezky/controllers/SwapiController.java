package ist.challenge.muhammad_rezky.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import ist.challenge.muhammad_rezky.services.SwapiService;

@RestController
public class SwapiController {

    private SwapiService service;

    @Autowired
    public SwapiController(SwapiService service){
        this.service = service;        
    }

    @GetMapping("/listPeople")
    public ResponseEntity<Object> listPeople(){
        try {
            return ResponseEntity.ok(service.listPeople());
        } catch (ResponseStatusException e) {
            return ResponseEntity.status(e.getStatusCode()).body(e.getReason());
        }
    }
}
