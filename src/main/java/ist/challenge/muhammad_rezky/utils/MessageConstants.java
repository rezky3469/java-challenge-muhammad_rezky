package ist.challenge.muhammad_rezky.utils;

public class MessageConstants {

    private MessageConstants(){        
    }

    public static final String SWAPI_URL = "https://swapi.py4e.com/api/people/";
    public static final String MESSAGE_CONFLICT = "Username sudah terpakai";
    public static final String MESSAGE_NOT_VALID = "Username / password tidak cocok";
    public static final String MESSAGE_NOT_FOUND = "User tidak ada";
    public static final String MESSAGE_LOGIN_SUCCESS = "Login berhasil";
    public static final String MESSAGE_USERNAME_NOT_BLANK = "Username tidak boleh kosong";
    public static final String MESSAGE_PASSWORD_NOT_BLANK = "Password tidak boleh kosong";
    public static final String MESSAGE_PASSWORD_MAX = "Password maximal terdiri dari 25 karakter";
    public static final String MESSAGE_USERNAME_MAX = "Username maximal terdiri dari 25 karakter";
    public static final String MESSAGE_USERNAME_ALREADY_USED = "Username sudah terpakai";
    public static final String MESSAGE_PASSWORD_ALREADY_USED = "Password tidak boleh sama dengan password sebelumnya";
}
